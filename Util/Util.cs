﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CidenetBackend.Util
{
    public class Util
    {
        public class HttpResponse<T>
        {
            public string? message { get; set; }
            public bool success { get; set; }
            public long? total { get; set; } = null;
            public T? data { get; set; }
        }

        public class HttpResponseList<T>
        {
            public string message { get; set; }
            public bool success { get; set; }
            public long total { get; set; }
            public List<T> data { get; set; }
        }
    }
}
