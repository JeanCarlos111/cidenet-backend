﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CidenetBackend.Data;
using CidenetBackend.Models;

namespace CidenetBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly DataContext _context;

        public EmployeesController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Employees
        [HttpGet]
        public async Task<Util.Util.HttpResponseList<Employees>> GetEmployees()
        {
            Util.Util.HttpResponseList<Employees> Data = new Util.Util.HttpResponseList<Employees>();
            try
            {
                if (_context.Employees == null)
                {
                    throw new Exception("Employees not found.");
                }

                Data.data = await _context.Employees.ToListAsync();
                Data.message = "";
                Data.success = true;

            } catch (Exception ex)
            {
                Data.data = null;
                Data.message = ex.Message;
                Data.success = false;
            }
            return Data;
        }
  

        // POST: api/list/Employees
        [HttpPost("list")]
        public async Task<Util.Util.HttpResponseList<Employees>> ListEmployees(int start, int limit, [FromBody] Employees values)
        {
            Util.Util.HttpResponseList<Employees> Data = new Util.Util.HttpResponseList<Employees>();
            try
            {

                if (_context.Employees == null)
                {
                    throw new Exception("Employees not found.");
                }
                var dataPagination = await _context.Employees.ToListAsync();

                if (!string.IsNullOrEmpty(values.FirstName))
                {
                   dataPagination = dataPagination.Where(e => e.FirstName.ToLower().Trim().Contains(values.FirstName.Trim().ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(values.OtherNames))
                {
                    dataPagination = dataPagination.Where(e => e.OtherNames.ToLower().Trim().Contains(values.OtherNames.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.FirstLastName))
                {
                    dataPagination = dataPagination.Where(e => e.FirstLastName.ToLower().Trim().Contains(values.FirstLastName.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.SecondLastName))
                {
                    dataPagination = dataPagination.Where(e => e.SecondLastName.ToLower().Trim().Contains(values.SecondLastName.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.IdType))
                {
                    dataPagination = dataPagination.Where(e => e.IdType.ToLower().Trim().Contains(values.IdType.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.IdNumber))
                {
                    dataPagination = dataPagination.Where(e => e.IdNumber.ToLower().Trim().Contains(values.IdNumber.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.CountryOfEmployment))
                {
                    dataPagination = dataPagination.Where(e => e.CountryOfEmployment.ToLower().Trim().Contains(values.CountryOfEmployment.ToLower().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(values.Email))
                {
                    dataPagination = dataPagination.Where(e => e.Email.ToLower().Trim().Contains(values.Email.ToLower().Trim())).ToList();
                }
                if (values.State)
                {
                    dataPagination = dataPagination.Where(e => e.State == values.State).ToList();
                }

                Data.data = dataPagination.Skip(start).Take(limit).ToList();
                Data.total = dataPagination.Count();
                Data.message = "";
                Data.success = true;
            }
            catch (Exception ex)
            {
                Data.data = null;
                Data.message = ex.Message;
                Data.success = false;
            }
            return Data;
        }

        // GET: api/Employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employees>> GetEmployees(int id)
        {
          if (_context.Employees == null)
          {
              return NotFound();
          }
            var employees = await _context.Employees.FindAsync(id);

            if (employees == null)
            {
                return NotFound();
            }

            return employees;
        }

        // PUT: api/Employees/5
        [HttpPut("{id}")]
        public async Task<Util.Util.HttpResponse<Employees>> PutEmployees(int id, Employees employees)
        {
            Util.Util.HttpResponse<Employees> Data = new Util.Util.HttpResponse<Employees>();
            try
            {
               if (id != employees.Id)
                {
                    throw new Exception("Employee does not exist.");
                }

                _context.Entry(employees).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                Data.data = employees;
                Data.message = "Employee modified successfully.";
                Data.success = true;
            } catch (Exception ex)
            {
                Data.data = null;
                Data.message = ex.Message;
                Data.success = false;
            }
            return Data;
        }

        // POST: api/Employees
        [HttpPost]
        public async Task<Util.Util.HttpResponse<Employees>> PostEmployees(Employees employees)
        {
            Util.Util.HttpResponse<Employees> Data = new Util.Util.HttpResponse<Employees>();
            try
            {
                if (_context.Employees == null)
                {
                    throw new Exception("Entity set 'DataContext.Employees'  is null.");
                }

                if (!ModelState.IsValid)
                {
                    throw new Exception("The values are invalid.");
                }

                _context.Employees.Add(employees);
                await _context.SaveChangesAsync();
                Data.data = employees;
                Data.message = "Employee added successfully.";
                Data.success = true;
            } catch (Exception ex)
            {
                Data.data = null;
                Data.message = ex.Message;
                Data.success = false;
            }
            return Data;
        }

        // DELETE: api/Employees/5
        [HttpDelete("{id}")]
        public async Task<Util.Util.HttpResponse<Employees>> DeleteEmployees(int id)
        {
            Util.Util.HttpResponse<Employees> Data = new Util.Util.HttpResponse<Employees>();
            try
            {
                if (_context.Employees == null)
                {
                    throw new Exception("Entity set 'DataContext.Employees'  is null.");
                }
                var employees = await _context.Employees.FindAsync(id);
                if (employees == null)
                {
                    throw new Exception("Employee does not exist.");
                }

                _context.Employees.Remove(employees);
                await _context.SaveChangesAsync();
                Data.data = employees;
                Data.message = "Employee delete successfully.";
                Data.success = true;

            } catch (Exception ex)
            {
                Data.data = null;
                Data.message = ex.Message;
                Data.success = false;
            }
            return Data;
        }

    
    }
}
