﻿using Microsoft.EntityFrameworkCore;

namespace CidenetBackend.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Models.Employees> Employees { get; set; }
    }
}
