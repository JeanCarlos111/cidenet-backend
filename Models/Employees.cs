﻿using System.ComponentModel.DataAnnotations;

namespace CidenetBackend.Models
{
    public class Employees
    {
        public int Id { get; set; }

        [StringLength(20)]
        public string FirstLastName { get; set; } = string.Empty;

        [StringLength(20)]
        public string SecondLastName { get; set; } = string.Empty;

        [StringLength(20)]
        public string FirstName { get; set; } = string.Empty;

        [StringLength(50)]
        public string OtherNames { get; set; } = string.Empty;

        public string CountryOfEmployment { get; set; } = string.Empty;

        public string IdType { get; set; } = string.Empty;

        [StringLength(20)]
        public string IdNumber { get; set; } = string.Empty;

        [StringLength(300)]
        public string Email { get; set; } = string.Empty;

        public DateTime StartDate { get; set; }

        public string Area { get; set; } = string.Empty;

        public bool State { get; set; } = true;
        public DateTime RegisterDate { get; set; } = DateTime.Now;
    }
}
